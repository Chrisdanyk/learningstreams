package cd.home;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static cd.home.Gender.FEMALE;
import static cd.home.Gender.MALE;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        List<Person> people = getPeople();
        List<Person> females = people.stream()
                .filter(person -> person.getGender().equals(FEMALE))
                .collect(Collectors.toList());
//        females.forEach(System.out::println);
        List<Person> sorted = people.stream()
                .sorted(Comparator.comparing(Person::getAge).reversed())
                .collect(Collectors.toList());
//        sorted.forEach(System.out::println);
        people.stream()
                .min(Comparator.comparing(Person::getAge));
//                .ifPresent(System.out::println);

        Map<Gender, List<Person>> groupByGender = people.stream().collect(Collectors.groupingBy(Person::getGender));
        groupByGender.forEach(((gender, peoples) -> {
            System.out.println(gender);
            peoples.forEach(System.out::println);
        }
        ));

    }

    public static List<Person> getPeople() {
        return List.of(new Person("James Bond", 20, MALE),
                new Person("Alina Smith", 33, FEMALE),
                new Person("Helen White", 57, FEMALE),
                new Person("Alex Boz", 14, MALE),
                new Person("Jamie Goa", 99, MALE),
                new Person("Anna Cook", 7, FEMALE),
                new Person("Zelda Brown", 120, FEMALE)
        );
    }


}
